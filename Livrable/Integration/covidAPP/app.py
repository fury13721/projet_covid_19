# -*- coding: utf-8 -*-

# Import du module Flask
from flask import Flask, render_template, render_template_string # pip install flask
#from bokeh.embed import server_document
import os, sys

# Initialisation de l'application
app = Flask(__name__)

# Ici on définit une page ou route avec Flask
# @app.route définit à quelle adresse tout ce qui suit va s'appliquer : page d'accueil "/"
@app.route('/')

# Vient ensuite la fonction hello qui va s'exécuter sur la page d'accueil "/"
def home():
    return render_template("home.html")#cette fonction évite que l'on code le HTML directement dans la fonction, on appelle un fichier HTML prévu pour le développpement.

# Ajout d'autres pages
@app.route('/bokeh')

def bokeh():
    return render_template("bokeh.html")

# Evite de relancer à chaque fois le serveur suite à une modification du fichier 'True' sinon mettre 'False'
# De même l'on peut changer le port : app.run(debug=True, port='3000') à la place de 5000
if __name__ == "__main__":
    app.run(debug=True)

